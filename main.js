const d = new Date();
const $input = document.querySelector('#input');
const $taskList = document.querySelector('#tasklist');
const $timeline = document.querySelector('#timeline');
const $tasksection = document.querySelector('#taskSection');
const $exportBtn = document.querySelector('button#export');
const $importBtn = document.querySelector('button#import');
const $saveBtn = document.querySelector('button#save');
const $importInput = document.querySelector('#importData');
const $changeHeight = document.querySelector('#timeline .changeSize');

const tot = $timeline.offsetTop; // tot = short für timeline Offset from top

let tasks = [];
let id = tasks.length;
let ld = new Date(1536260354779);
let colors = ['A2238E','ED008C','F9A01E','F8EE2C','C4D72D','4CB848','19C1F2','0083CB','0154A4'];
let colorID = -1;

let da = false // drag active
let ww = window.innerWidth // windowWidth
let gw = ww/36 // good width
let at = 0 // active task
let tsn = 0 //task start number

$input.addEventListener('change', addItemEventHandler);

function addItemEventHandler(e) {
  let content = e.target.value.trim();
  if (content) {
    addItem(content,id);
  }
}
function addItem(content,pid = id,create = true) {
      $taskList.innerHTML += '<li id="tl'+pid+'"><span class="splash"></span>'+content+'<span class="to">8:30</span><span class="from">8:15</span></li>';
      if (create) {
        addTask($input.value,pid,function(task) {
          initTaskView(task.id);
        });
      }else {
        initTaskView(pid);
      }
      $input.value = '';
      if (pid == id) {
        id++;
      }
      for (var i = 0; i < document.querySelectorAll('.task').length; i++) {
        document.querySelectorAll('.taskEA')[i].style.height = (100/id)+"%";
      }
}

function initTaskView(id) {
  let task = tasks[id];

  // create element if it doesn't exist
  if (!document.getElementById('#t'+task.id)) {
    createTaskNode(task);
  }
  // place it on gant diagram
  renderTask(task.id);
  // add addEventListener
  let taskEA = document.querySelector('#tea'+task.id);
  // let  taskListElm = document.querySelector('#tl'+task.id);
  let  taskListElm = document.querySelector('#tl'+task.id);
  console.log(taskListElm);
  taskEA.addEventListener('mousedown', function(e) {
    editTask(task.id,e);
    at = task.id;
    da = true;
    tsn = coolNumber(e.pageX,gw);
  });
  taskListElm.addEventListener('mousedown',function(e) {
    at = task.id;
    console.log(at);
  });
}

// if mouse down, edit the values of the selected task
function editTask(id,e) {
  let task = tasks[id];
  let tl =  coolNumber(e.pageX,gw)- tsn;
  if (tl < 0) {
    task.tlength = Math.abs(tl) + 1;
    task.from = tsn + tl;
  }else {
    task.tlength = tl +1;
    task.from = tsn;
  }
  renderTask(task.id);
  editTaskText(task.id);
}

function editTaskText(id) {
  let task = tasks[id];
  let $from = document.querySelector('#tl'+id+' .from');
  let $to = document.querySelector('#tl'+id+' .to');
  $from.innerHTML = bTime(task.from);
  $to.innerHTML = bTime(task.from+task.tlength);
}

function bTime(ph,ft=8,showAsTime=false) {
  ph = ph/4;
  let h = Math.floor(ph);
  let min = (ph-h)*60;
  min = (min%60==0)? min+'0': min;
  if (showAsTime) {
    let rw = ""
    if (h>0) {
      rw += (ft+h)+" Std";
    }
    if (h>0 && min !== "00") {
      rw += ", ";
    }
    if (min !== "00") {
      rw +=min+" Min.";
    }
    return rw;
  }
  return (ft+h)+":"+min;
}

// from Dataset to Gant;
function renderTask(id) {
  let task = tasks[id];
  let taskElm = document.querySelector('#t'+task.id);
  taskElm.style.marginLeft = task.from*gw+"px";
  taskElm.style.width = task.tlength*gw+"px";
}

function createTaskNode(task) {
  let taskElm = document.createElement('div');
  taskElm.id = 'tea'+task.id;
  taskElm.className = 'taskEA';
  $tasksection.appendChild(taskElm);
  let taskItem = document.createElement('div');
  taskItem.id = 't'+task.id;
  taskItem.className = 'task';
  taskItem.style.background = "#"+task.color;
  taskElm.appendChild(taskItem)
  // adding the splash
  let $splash = document.querySelector('#tl'+task.id+' .splash');
  $splash.style.background = "#"+task.color;
}

function addTask(pContent,pId,f) {
  let task = {
    id:pId,
    content: pContent.trim(),
    tlength: 1,
    from: 1,
    color: genColor()
  }
  tasks.push(task);
  f(task);
}

function genColor() {
  colorID++;
  colorID = colorID >= colors.length ? 0 : colorID;
  return colors[colorID];
}


// addEventListeners
// document.addEventListener('mousedown', md);
document.addEventListener('mousemove', mm);
document.addEventListener('mouseup', mu);
document.addEventListener('mouseup', mu);
window.addEventListener('resize',resizer);
$changeHeight.addEventListener('mousedown',ch);
$exportBtn.addEventListener('click', openExportWindow);
$importBtn.addEventListener('click', function() {$importInput.click();});
$importInput.addEventListener('change',importData);
$saveBtn.addEventListener('click',saveToFile);

// function to change the height
function ch(e) {
  da = true;
  at = 'chth'; // change timeline Height
}

// function for resize
function resizer() {
  ww = window.innerWidth;
  gw = ww/36;
  for (var i = 0; i < tasks.length; i++) {
    renderTask(tasks[i].id);
  }
}

// function for mouseDown
function md(e) {
  tew = gw;
  da = true;
  tesn = nextNumber(e.pageX,gw);
  $te.style.marginLeft = tesn;
  $te.style.width = tew;
}

// function for mousemove
function mm(e) {
  if (da) {
    if (typeof at === 'number') {
      editTask(at,e)
    }else if (at == 'chth') {
      $timeline.style.height = e.pageY-tot+"px";
    }
  }
}

// function for mouseup
function mu(e) {
  da = false;
}

function coolNumber(x,y) {
  return Math.round(nextNumber(x,y)/y);
}

function nextNumber(x,y) {
  return x - x%y;
}

function openExportWindow() {

  let newWindow = window.open("", null, "height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
  let exportString = "";
  let exportTasks = Array();
  for (var i = 0; i < tasks.length; i++) {
    let task = tasks[i];
    let et = {}
    et.from = bTime(task.from);
    et.to = bTime(task.from+task.tlength);
    et.content = task.content;
    et.tlength = bTime(task.tlength-overlap(i),0,true);
    exportTasks.push(et);
    exportString += "<tr>"+
                      eVal(et.from+" - "+et.to)+
                      eVal(et.content)+
                      eVal(et.tlength)+
                      "</tr>";
  }
  newWindow.document.write('<h1>in Input fields</h1>')
  newWindow.document.write("<table>"+exportString+"</table>");
  newWindow.document.write('<hr><h1>JSON (with rendered Time)</h1>')
  newWindow.document.write("<textarea>"+JSON.stringify(exportTasks)+"</textarea>");
  newWindow.document.write('<hr><h1>JSON (import raw format)</h1>')
  newWindow.document.write("<textarea>"+JSON.stringify(tasks)+"</textarea>");
  newWindow.document.close();
}
function eVal(e) {
  return '<td><input value="'+e+'"></td>';
}
function overlap(id) {
  let ro = 0;
  let task = tasks[id];
  var free = [0,8,16,17,18,27];
  for (var i = 0; i < task.tlength; i++) {
    if (free.includes(task.from +i)) {
      ro++
    }
  }
  return ro;
}

function saveToFile() {
  var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(tasks));
  var downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute("href",     dataStr);
    downloadAnchorNode.setAttribute("download", 'tasks' + ".json");
    document.body.appendChild(downloadAnchorNode); // required for firefox
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
}

function importData(e) {
  let file = $importInput.files[0];
  if (file.type == "application/json") {
    if (tasks.length == 0) {

                let reader = new FileReader();

                reader.onload = function(e2) {
                    // finished reading file data.
                    console.log(e2.target.result);
                    tasks = JSON.parse(e2.target.result);
                    for (var i = 0; i < tasks.length; i++) {
                      addItem(tasks[i].content,i,false);
                    }
                }
                reader.readAsText(file); // start reading the file data.
      }else {
        alert('Seite neu laden um zu Importieren. (ACHTUNG All deine Veränderungen gehen verloren)');
      }
  }else {
    alert('du musst ein json importieren')
  }
}
